<?php
namespace ImportProducts;

use OneIota\Exporters\JsonExporter;
use OneIota\Formatters\CsvFormatter;
use OneIota\Import;
use OneIota\Importers\CsvImporter;
use OneIota\Uploader;
use OneIota\Log;

/**
 * Simulates a real life app
 * Class App
 * @package ImportProducts
 */
class App
{
    /**
     * @var Uploader
     */
    protected $uploader;

    /**
     * App constructor.
     */
    function __construct()
    {
        Log::init(LOG_PATH);
        $this->uploader = new Uploader();
    }

    /**
     * Simulate a real life import scenario
     * @return string
     */
    public function run()
    {
        Log::add('Began Import');
        //get uploaded file
        $uploadedFile = $this->uploader->getUpload();
        //prepare importer and exporter classes
        $importer = new CsvImporter();
        $importer->setFile($uploadedFile);
        $exporter = new JsonExporter();
        $formatter = new CsvFormatter();
        //perform new import
        $import = new Import($importer, $exporter, $formatter);
        $importedData = $import->getInformation();
        $exportFile = JSON_STORAGE_PATH . DIRECTORY_SEPARATOR . date('Y-m-d') . '.json';
        file_put_contents($exportFile, $importedData);
        echo 'Done! You can find the exported file here: ' . $exportFile;
        Log::add('Import done!');
    }

}