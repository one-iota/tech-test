#The storage folder

* one_iota.log - logfile for the app
* json/*.json - imported json files. I have not validated this, to check if a previous import with
the same name exists and timestamp or id them somehow - not relevant for a technical test.