<?php
//bootstrap dependencies
require_once '../vendor/autoload.php';

//Define required contsants
define('UPLOAD_PATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR));
define('JSON_STORAGE_PATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'json' . DIRECTORY_SEPARATOR));
define('LOG_PATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'one_iota.log'));

//run app
use ImportProducts\App;

$app = new App;
$app->run();