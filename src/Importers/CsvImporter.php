<?php
namespace OneIota\Importers;

use OneIota\ExporterInterface;
use OneIota\ImporterInterface;

/**
 * Class CsvImporter
 * @package OneIota\Importers
 */
class CsvImporter implements ImporterInterface
{
    /**
     * CSV File to be processed
     * @var null | string
     */
    protected $file = null;

    /**
     * Stores the processed data
     * @var array
     */
    protected $data = array();

    /**
     * CSV Parser used for data processing
     * @var \parseCsv
     */
    protected $importer;

    /**
     * Loads dependencies into members
     * CsvImporter constructor.
     */
    public function __construct()
    {
        $this->importer = new \parseCSV;
        $this->importer->heading = false;
        return $this;
    }

    /**
     * @return array if successful
     * @throws \Exception if class has not been initialized properly
     */
    public function getData()
    {
        if ($this->file === null) {
            throw new \Exception('CSV Importer not initialized properly!');
        }
        $this->importer->auto($this->file);
        $this->data = $this->importer->data;
        return $this->data;
    }

    /**
     * Sets the csv file to be imported
     * @param $filePath
     * @throws \Exception if file does not exist
     * @return ExporterInterface
     */
    public function setFile($filePath)
    {
        if (!file_exists($filePath)) {
            throw new \Exception('Provided file does not exist!');
        }
        $this->file = $filePath;
    }
}