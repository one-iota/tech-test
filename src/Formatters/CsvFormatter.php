<?php
namespace OneIota\Formatters;

use OneIota\FormatterInterface;

/**
 * Class used for formatting imported data to usable information
 * Class Formatter
 * @package OneIota
 */
class CsvFormatter implements FormatterInterface
{
    /**
     * Used to store the size comparison chart grouped by types
     * @var array
     */
    protected $comparisonChart = [];

    /**
     * Used to store the formatted information
     * @var array
     */
    protected $formattedData = [];

    /**
     * Formatter constructor.
     */
    public function __construct()
    {
        $this->comparisonChart = include(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'comparisonChart.php');
    }

    /**
     * Formats the data inputted according to specifications
     * @param array $rawData
     * @return array
     */
    public function format(array $rawData)
    {
        $this->groupInitialData($rawData);
        $this->sortDataBySize();
        return $this->formattedData;
    }

    /**
     * Groups initial data to the desired format
     * @param $data
     */
    protected function groupInitialData($data)
    {
        foreach ($data as $row) {
            //trim whitespace
            $row = array_map('trim', $row);
            //create individual PLU array if not exists
            if (!isset($this->formattedData[$row[1]])) {
                $this->formattedData[$row[1]] = [
                    'PLU' => $row[1],
                    'name' => $row[2],
                    'sizes' => []
                ];
            }
            $this->formattedData[$row[1]]['sizes'][$this->comparisonChart[$row[4]][$row[3]]] = [
                'SKU' => $row[0],
                'size' => $row[3]
            ];
        }
    }

    /**
     * Sorts the $formattedData member's sizes
     * @return mixed
     */
    protected function sortDataBySize()
    {
        foreach ($this->formattedData as $plu => $pluData) {
            ksort($this->formattedData[$plu]['sizes']);
            $this->formattedData[$plu]['sizes'] = array_values($this->formattedData[$plu]['sizes']);
        }
        $this->formattedData = array_values($this->formattedData);
    }
}