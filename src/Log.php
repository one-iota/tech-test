<?php
namespace OneIota;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Class Log
 * Used to log the actions performed to a log
 * @package OneIota
 */
class Log
{
    /**
     * @var Logger
     */
    private static $logger = null;

    /**
     * Stores the log file location
     * @var string
     */
    private static $logFile = null;

    /**
     * Log constructor. Private, doesn't need to live on its own.
     */
    private function __construct()
    {
    }

    /**
     * Initializes the Log class
     * @param $logFile
     */
    public static function init($logFile)
    {
        self::$logFile = $logFile;
        self::$logger = new Logger('OneIota');
        self::$logger->pushHandler(new StreamHandler(self::$logFile, Logger::INFO));
    }

    /**
     * Clears the log
     * @throws \Exception if Log class is not instantiated
     */
    public static function clear()
    {
        self::checkInit();
        file_put_contents(self::$logFile, '');
    }

    /**
     * Checks if Log class is properly initialized
     * @throws \Exception
     */
    protected static function checkInit()
    {
        if (self::$logger == null) {
            throw new \Exception('Logger not initialized properly!');
        }
    }

    /**
     * Logs the text to the configured file
     * @param $text
     * @throws \Exception if Log class is not instantiated
     */
    public static function add($text)
    {
        self::checkInit();
        self::$logger->info($text);
    }
}