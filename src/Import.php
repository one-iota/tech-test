<?php
namespace OneIota;


class Import
{
    /**
     * Importer Class
     * @var ImporterInterface
     */
    protected $importer;
    /**
     * Exporter Class
     * @var ExporterInterface
     */
    protected $exporter;

    /**
     * Used for formatting the results
     * Did not add it through dependency inj...
     * @var FormatterInterface
     */
    protected $formatter;

    /**
     * Stores the imported data
     * @var array
     */
    protected $data = array();

    /**
     * Import constructor.
     * @param ImporterInterface $importer
     * @param ExporterInterface $exporter
     * @param FormatterInterface $formatter
     */
    public function __construct(ImporterInterface $importer, ExporterInterface $exporter, FormatterInterface $formatter)
    {
        $this->importer = $importer;
        $this->exporter = $exporter;
        $this->formatter = $formatter;
    }

    /**
     * Returns the imported information, formatted
     * @return array
     */
    public function getInformation()
    {
        $this->data = $this->formatter->format($this->importer->getData());
        return $this->exporter->export($this->data);
    }
}