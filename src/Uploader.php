<?php
namespace OneIota;

/**
 * Simulates a file upload class
 * Class Uploader
 * @package OneIota
 */
class Uploader
{
    /**
     * Simulates a successful upload
     * @return string
     */
    public function getUpload()
    {
        return UPLOAD_PATH . DIRECTORY_SEPARATOR . 'csv' . DIRECTORY_SEPARATOR . 'products.csv';
    }
}