<?php
namespace OneIota;

/**
 * Interface used when formatting data
 * Interface ExporterInterface
 * @package OneIota
 */
interface FormatterInterface
{
    /**
     * Formats the given data
     * @param array $rawData
     * @return mixed
     */
    public function format(array $rawData);
}