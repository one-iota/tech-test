<?php
namespace OneIota;

/**
 * Interface used when exporting data
 * Interface ExporterInterface
 * @package OneIota
 */
interface ExporterInterface
{
    public function export(array $data);
}