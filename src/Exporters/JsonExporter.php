<?php
namespace OneIota\Exporters;

use OneIota\ExporterInterface;

/**
 * Json Exporter Class
 * Class JsonExporter
 * @package OneIota\Exporters
 */
class JsonExporter implements ExporterInterface
{
    public function export(array $data)
    {
        return json_encode($data);
    }
}