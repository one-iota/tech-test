<?php
namespace OneIota;

/**
 * Interface used when importing data
 * Interface ImporterInterface
 * @package OneIota
 */
interface ImporterInterface
{
    /**
     * Returns Data array
     * @return array
     */
    public function getData();

}