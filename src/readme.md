#The source code

The organisation of the src/ folder adheres to the business logic provided in the job brief.
The codebase namespace is OneIota.

##Import.php

Main import class. Uses dependency injection to load an importer, an exporter and a formatter, used to
perform a successful import. Provides enough flexibility to be extended in the future, to import
a variety of data and serve it back, in any format.

##Interfaces

To create dependency injection, i have created `ImporterInterface.php`, `ExporterInterface.php` and
`FormatterInterface.php`. These are the contracts the importers/exporters/formatters hae to abide.

I have created a class for each of the interfaces, to create the required functionality. You can find them in
 `src/Importers`, `src/Exporters` and `src/Formatters`.

##Uploader.php

Simulates a file upload, to make the demo look more like a real life scenario.

##src/config folder
The folder only contains a config file , `comparisonchart.php`, that stores the sorting table --
this can be a db table / config / cached config - but for a demo, a config file is sufficient, i believe.

##Log class - used for demo
`Log.php` - Class used to do logging, it is just a wrapper for the Monolog class found in the
vendor folder, for simplicity.




