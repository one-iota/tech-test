#One Iota Technical Test

**Name:** Razvan Cristian Marian

**Email:** [razvan.marian@igeek.ro](razvan.marian@igeek.ro)

**Website:** [http://www.ied3vil.com](http://www.ied3vil.com)

##Information

Code Location: `src/`

I have also created a `public` folder and an `app` folder to simulate a real life scenario.

Running the app currently just dumps the resulted json in `storage/json/Y-m-d.json` and outputs a "Done" message.
I have included a sample result of a demo in the `2016-08-20-sample.json` file
(I have used my IDE to format it so it's easier to read).

For more clarity, **i have added a number of readme files** to help understand what you are looking at
when browsing the repository, in relevant folders.

##To run a demo
Just clone the code, run `composer update` and access `public/index.php` in the browser.
I have included the vendor folder in the repo just so you can skip the composer update part. Also,
make sure the `storage/` folder is writable by the webserver (or php if you run index.php in the command line).